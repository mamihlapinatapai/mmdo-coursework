import React from 'react';
import MathProgrammingForm from './components/math-programming-form';

function App() {
  return (
    <div className="my-8">
      <MathProgrammingForm />
    </div>
  );
}

export default App;
