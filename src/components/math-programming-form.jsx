import React from 'react';
import { Button, TextInput } from 'flowbite-react';
import {
  addColumn,
  addRow,
  removeColumn,
  removeRow,
  solve,
} from '../utils/math-programming-solver';

function MathProgrammingForm() {
  const handleSubmit = (e) => {
    e.preventDefault();

    solve(
      e.target.elements.xValue,
      e.target.elements.constrainValue,
      e.target.elements.coefficients
    );
  };
  return (
    <div className="container pt-4">
      <h1 className="text-2xl">Курсова робота. Метод симплекс-таблиць</h1>
      <form className="data pt-5" onSubmit={handleSubmit}>
        <div className="table-responsive pt-4">
          <table className="inputTable table table-bordered border-gray-300 align-middle text-center">
            <tbody>
              <tr>
                <td
                  style={{
                    borderTopLeftRadius: '30px',
                    borderTop: 'none',
                    background: '#9ca3af',
                  }}
                  rowSpan="2"
                  className="bg-slate-800"
                >
                  Обмеження
                </td>
                <td style={{ background: '#9ca3af' }} colSpan="2">
                  Змінні
                </td>
                <td
                  style={{
                    borderTopRightRadius: '30px',
                    background: '#9ca3af',
                  }}
                  rowSpan="2"
                >
                  Вільні члени
                </td>
              </tr>
              <tr className="vars" style={{ background: '#6b7280' }}>
                <td style={{ background: '#9ca3af' }}>
                  x<sub>1</sub>
                </td>
                <td style={{ background: '#9ca3af' }}>
                  x<sub>2</sub>
                </td>
              </tr>
              <tr>
                <td style={{ background: '#9ca3af' }}>
                  <b>1</b>
                </td>
                <td style={{ background: '#9ca3af' }}>
                  <TextInput type="text" name="xValue" defaultValue="0" />
                </td>
                <td style={{ background: '#9ca3af' }}>
                  <TextInput type="text" name="xValue" defaultValue="0" />
                </td>
                <td style={{ background: '#9ca3af' }}>
                  <TextInput
                    type="text"
                    name="constrainValue"
                    defaultValue="0"
                  />
                </td>
              </tr>
              <tr>
                <td style={{ background: '#9ca3af' }}>
                  <b>2</b>
                </td>
                <td style={{ background: '#9ca3af' }}>
                  <TextInput type="text" name="xValue" defaultValue="0" />
                </td>
                <td style={{ background: '#9ca3af' }}>
                  <TextInput type="text" name="xValue" defaultValue="0" />
                </td>
                <td style={{ background: '#9ca3af' }}>
                  <TextInput
                    type="text"
                    name="constrainValue"
                    defaultValue="0"
                  />
                </td>
              </tr>
              <tr className="coefficients">
                <td
                  style={{
                    borderBottomLeftRadius: '30px',
                    borderBottom: 'none',
                    background: '#9ca3af',
                  }}
                >
                  F ={' '}
                </td>
                <td style={{ background: '#9ca3af' }}>
                  <TextInput type="text" name="coefficients" defaultValue="0" />
                </td>
                <td style={{ background: '#9ca3af' }}>
                  <TextInput type="text" name="coefficients" defaultValue="0" />
                </td>
                <td
                  style={{
                    borderBottomRightRadius: '30px',
                    borderBottom: 'none',
                    background: '#9ca3af',
                  }}
                  className="anchor"
                >
                  max
                </td>
              </tr>
            </tbody>
          </table>
        </div>

        <div className="flex justify-center gap-24 mt-4">
          <div className="flex flex-col">
            <div className="flex gap-4 mb-3">
              <Button
                className="w-[210px]"
                color="success"
                pill
                onClick={addColumn}
              >
                <p>Додати змінну</p>
              </Button>
              <Button
                className="w-[210px]"
                color="failure"
                pill
                onClick={removeColumn}
              >
                <p>Видалити змінну</p>
              </Button>
            </div>

            <div className="flex gap-4">
              <Button
                className="w-[210px]"
                color="success"
                pill
                onClick={addRow}
              >
                <p>Додати обмеження</p>
              </Button>
              <Button
                className="w-[210px]"
                color="failure"
                pill
                onClick={removeRow}
              >
                <p>Видалити обмеження</p>
              </Button>
            </div>

            <Button className="w-[28rem] mt-4" pill type="submit">
              Розрахувати
            </Button>
          </div>
        </div>
      </form>
      <div className="result table-responsive mt-5"></div>
      <div>
        <p></p>
      </div>
    </div>
  );
}

export default MathProgrammingForm;
