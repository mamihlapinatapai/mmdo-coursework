import Fraction from 'fraction.js';

let columnCount = 2;
let rowCount = 2;

export function solve(xInput, constrainsInput, coefficientsInput) {
  document.querySelector('.result').innerHTML = ``;

  //Free coefficients
  let constrains = Array.from(constrainsInput);
  constrains = constrains.map((elem) => new Fraction(elem.value));

  //Main function coefficients
  let coefficients = Array.from(coefficientsInput);
  coefficients = coefficients.map((elem) => new Fraction(elem.value));

  let xUnformated = Array.from(xInput);
  xUnformated = xUnformated.map((elem) => elem.value);

  //Constrains coefficients
  let xValues = [];
  for (let i = 0; i < xUnformated.length; i += columnCount) {
    xValues.push(xUnformated.slice(i, i + columnCount));
  }

  //Merge coefficients and add new for basis vars
  for (let i = 0; i < xValues.length; i++) {
    xValues[i].push(new Fraction(constrains[i]));
    coefficients.push(new Fraction(0));
  }

  //Middle of the first simplex table
  let mainMatrix = [];
  xValues.map((constrainCoefficeint, i) => {
    //Basis var coeficient
    let matrixRow = [new Fraction(0)];

    //Basis var
    matrixRow.push(`x${i + columnCount + 1}`);

    //Free coeficients
    matrixRow.push(
      new Fraction(constrainCoefficeint[constrainCoefficeint.length - 1])
    );

    //The rest of them
    for (let i = 0; i < constrainCoefficeint.length - 1; i++) {
      matrixRow.push(new Fraction(constrainCoefficeint[i]));
    }

    //Coefficients of the basis/rest of the vars
    for (let j = 0; j < xValues.length; j++) {
      if (j === i) {
        matrixRow.push(new Fraction(1));
      } else {
        matrixRow.push(new Fraction(0));
      }
    }

    //Apend row to the first matrix
    mainMatrix.push(matrixRow);
  });

  //Main loop
  for (let i = 0; i < 20; ++i) {
    //Weights row
    let weights = [];
    let mainWeight = new Fraction(0);
    for (let i = -1; i < mainMatrix[0].length - 3; i++) {
      let sum = new Fraction(0);
      for (let j = 0; j < mainMatrix.length; j++) {
        sum = sum.add(mainMatrix[j][i + 3].mul(mainMatrix[j][0]));
      }

      if (i === -1) {
        //For free coefficient row
        mainWeight = sum;
      } else {
        //For everything else
        weights.push(sum.sub(coefficients[i]));
      }
    }

    //Check exit conditions
    let finish = checkFinish(mainMatrix, weights);
    if (finish || finish == -1) {
      printMatrix(mainMatrix, coefficients, weights, mainWeight, 0, 0, true);

      if (finish == -1) {
        mainWeight = new Fraction(-1);
      }

      printResult(mainMatrix, mainWeight);
      break;
    }

    //3 because data in the matrix represented as [var coefficient, name of the var, free coefficient row, the rest...]
    let guideColumn = 3;

    //Finding guide column
    weights.map((value, i) => {
      if (
        value.compare(weights[guideColumn - 3]) == -1 &&
        value.compare(0) == -1
      ) {
        guideColumn = i + 3;
      }
    });

    //Guide row check for devision by 0
    let guideRow = 0;
    let minForGuideRow = new Fraction(Number.MAX_SAFE_INTEGER);
    try {
      mainMatrix[0][2].div(mainMatrix[0][guideColumn]);
    } catch {
      minForGuideRow = new Fraction(Number.MAX_SAFE_INTEGER);
    }

    //Mark first division as invalid if non-positive
    if (minForGuideRow.compare(0) == -1 || minForGuideRow.compare(0) == 0) {
      minForGuideRow = new Fraction(Number.MAX_SAFE_INTEGER);
    }

    //Finding guide row
    mainMatrix.map((value, i) => {
      let currentDivision = new Fraction(Number.MAX_SAFE_INTEGER);
      try {
        currentDivision = value[2].div(value[guideColumn]);
      } catch {
        currentDivision = new Fraction(Number.MAX_SAFE_INTEGER);
      }
      if (
        currentDivision.compare(minForGuideRow) == -1 &&
        (currentDivision.compare(0) == 1 || currentDivision.compare(0) == 0)
      ) {
        guideRow = i;
        minForGuideRow = currentDivision;
      }
    });

    printMatrix(
      mainMatrix,
      coefficients,
      weights,
      mainWeight,
      guideColumn,
      guideRow
    );
    //Calculate next matrix
    mainMatrix = transformToTheNextMatrix(
      mainMatrix,
      coefficients,
      xValues,
      guideRow,
      guideColumn
    );
  }
}

export function transformToTheNextMatrix(
  matrix,
  coefficients,
  xValues,
  guideRow,
  guideColumn
) {
  const guideElement = matrix[guideRow][guideColumn];
  //Finding elements of guide row
  for (let i = 2; i < matrix[guideRow].length; i++) {
    matrix[guideRow][i] = matrix[guideRow][i].div(guideElement);
  }

  //Finding everything else
  matrix.map((row, i) => {
    if (i !== guideRow) {
      let elem = row[guideColumn];
      for (let j = 2; j < row.length; ++j) {
        matrix[i][j] = matrix[i][j].sub(elem.mul(matrix[guideRow][j]));
      }
    }
  });

  //Replace old var and it's coefficient based on guide column
  matrix[guideRow][0] = coefficients[guideColumn - 3];
  matrix[guideRow][1] =
    guideColumn - 2 <= coefficients.length - xValues.length
      ? `x${guideColumn - 2}`
      : `x${guideColumn - xValues[0].length + columnCount}`;
  return matrix;
}

export function checkFinish(matrix, weights) {
  for (let i = 0; i < weights.length; i++) {
    let checkMinus = true;
    for (let j = 0; j < matrix.length; j++) {
      if (matrix[j][i + 3].compare(0) == 1) {
        checkMinus = false;
        break;
      }
    }

    if (
      checkMinus &&
      (weights[i].compare(0) == -1 || weights[i].compare(0) == 0)
    ) {
      //Specific exit if no solutions
      return -1;
    }
  }

  for (let i = 0; i < weights.length; i++) {
    if (weights[i].compare(0) == -1) {
      return false;
    }
  }
  //Exit if solutions are found
  return true;
}

export function printResult(matrix, mainWeight) {
  let result = document.createElement('div');
  result.classList.add('mb-6', 'text-xl', 'text-center');
  if (mainWeight.compare(-1) == 0) {
    result.innerHTML = `Розв'язків не існує`;
    document.querySelector('.result').append(result);
    return;
  }

  let res = mainWeight.toFraction();
  result.innerHTML = `<hr class="my-12"/>`;
  result.innerHTML += `<div>Результат: <br/></div>`;

  for (let i = 0; i < columnCount; i++) {
    let value = new Fraction(0);
    for (let j = 0; j < matrix.length; j++) {
      if (matrix[j][1] == `x${i + 1}`) {
        value = matrix[j][2];
      }
    }
    result.innerHTML += `<p class="my-4">x<sub>${
      i + 1
    }</sub> = ${value.toFraction()} </p>`;
  }
  result.innerHTML += `<p>F<sub>max</sub> = ${res}</p>`;
  document.querySelector('.result').append(result);
}

export function printMatrix(
  matrix,
  cRow,
  weights,
  mainWeight,
  guideColumn,
  guideRow,
  last = false
) {
  let tableOuter = document.createElement('table');
  tableOuter.classList.add(
    'table',
    'table-bordered',
    'border-gray-300',
    'align-middle',
    'text-center',
    'mt-4',
    'rounded-circle'
  );
  let table = document.createElement('tbody');

  let heading = document.createElement('tr');

  let coefficients = document.createElement('td');
  coefficients.innerHTML = 'Коефіцієнти';
  coefficients.rowSpan = 2;
  coefficients.style.background = '#9ca3af';
  coefficients.style.width = '15%';
  heading.append(coefficients);

  let c = document.createElement('td');
  c.innerHTML = 'C';
  c.style.background = '#9ca3af';
  heading.append(c);

  let first = document.createElement('td');
  first.innerHTML = '-';
  first.style.background = '#9ca3af';
  heading.append(first);

  for (let i = 0; i < columnCount + rowCount; i++) {
    let td = document.createElement('td');
    td.innerHTML = cRow[i];
    td.style.background = '#9ca3af';
    heading.append(td);
  }
  table.append(heading);

  let bRow = document.createElement('tr');
  let b = document.createElement('td');
  b.style.background = '#9ca3af';
  b.innerHTML = 'B';
  bRow.append(b);
  for (let i = 0; i <= columnCount + rowCount; i++) {
    let td = document.createElement('td');
    td.style.background = '#9ca3af';
    td.innerHTML = `A<sub>${i}</sub>`;
    bRow.append(td);
  }
  table.append(bRow);

  for (let i = 0; i < matrix.length; i++) {
    let row = document.createElement('tr');

    for (let j = 0; j < matrix[i].length; j++) {
      let td = document.createElement('td');
      td.style.background = '#9ca3af';

      if (j == 1) {
        td.innerHTML = matrix[i][j];
      } else {
        td.innerHTML = matrix[i][j].toFraction();

        if (j !== 0 && !last) {
          if (i === guideRow || j === guideColumn) {
            td.style.backgroundColor = '#4ade80';
          }

          if (i === guideRow && j === guideColumn) {
            td.style.backgroundColor = '#166534';
            td.classList.add('font-bold');
            td.classList.add('text-white');
          }
        }
      }

      row.append(td);
    }
    table.append(row);
  }

  let weightsRow = document.createElement('tr');
  let mainWeightTd = document.createElement('td');
  let delta = document.createElement('td');
  let td = document.createElement('td');
  delta.style.background = '#9ca3af';
  td.style.background = '#9ca3af';

  mainWeightTd.style.background = '#9ca3af';

  mainWeightTd.innerHTML = mainWeight.toFraction();
  delta.innerHTML = `∆`;
  weightsRow.append(td);
  weightsRow.append(delta);
  weightsRow.append(mainWeightTd);

  for (let i = 0; i < weights.length; i++) {
    let td = document.createElement('td');
    td.style.background = '#9ca3af';

    td.innerHTML = weights[i].toFraction();
    weightsRow.append(td);
  }
  table.append(weightsRow);
  tableOuter.append(table);

  document.querySelector('.result').append(tableOuter);
}

export function addRow() {
  if (rowCount >= 10) {
    return;
  } else {
    ++rowCount;
  }

  let row = document.createElement('tr');
  let heading = document.createElement('td');
  heading.style.background = '#9ca3af';

  heading.innerHTML = `<b>${rowCount}</b>`;
  row.append(heading);

  for (let i = 0; i < columnCount; i++) {
    let elem = document.createElement('td');
    elem.style.background = '#9ca3af';

    elem.innerHTML = `<div class="flex"><div class="relative w-full"><input class="block w-full border disabled:cursor-not-allowed disabled:opacity-50 bg-gray-50 border-gray-300 text-gray-900 focus:border-cyan-500 focus:ring-cyan-500 dark:border-gray-600 dark:bg-gray-700 dark:text-white dark:placeholder-gray-400 dark:focus:border-cyan-500 dark:focus:ring-cyan-500 rounded-lg p-2.5 text-sm" type="text" name="xValue" value="0"></div></div>`;
    row.append(elem);
  }

  let constrain = document.createElement('td');
  constrain.style.background = '#9ca3af';

  constrain.innerHTML = `<div class="flex"><div class="relative w-full"><input class="block w-full border disabled:cursor-not-allowed disabled:opacity-50 bg-gray-50 border-gray-300 text-gray-900 focus:border-cyan-500 focus:ring-cyan-500 dark:border-gray-600 dark:bg-gray-700 dark:text-white dark:placeholder-gray-400 dark:focus:border-cyan-500 dark:focus:ring-cyan-500 rounded-lg p-2.5 text-sm" type="text" name="constrainValue" value="0"></div></div>`;
  row.append(constrain);
  document.querySelector('.coefficients').before(row);
}

export function removeRow() {
  if (rowCount <= 2) {
    return;
  } else {
    --rowCount;
  }

  document.querySelector('.coefficients').previousSibling.remove();
}

export function addColumn() {
  if (columnCount >= 10) {
    return;
  } else {
    ++columnCount;
  }

  document.querySelector(
    '.inputTable'
  ).children[0].children[0].children[1].colSpan = columnCount;

  let heading = document.createElement('td');
  heading.style.background = '#9ca3af';

  heading.innerHTML = `x<sub>${columnCount}</sub>`;
  document.querySelector('.vars').append(heading);

  let row = document.querySelector('.vars').nextElementSibling;
  for (let i = 0; i < rowCount; i++) {
    let elem = document.createElement('td');
    elem.style.background = '#9ca3af';

    elem.innerHTML = `<div class="flex"><div class="relative w-full"><input class="block w-full border disabled:cursor-not-allowed disabled:opacity-50 bg-gray-50 border-gray-300 text-gray-900 focus:border-cyan-500 focus:ring-cyan-500 dark:border-gray-600 dark:bg-gray-700 dark:text-white dark:placeholder-gray-400 dark:focus:border-cyan-500 dark:focus:ring-cyan-500 rounded-lg p-2.5 text-sm" type="text" name="xValue" value="0"></div></div>`;
    row.children[row.children.length - 2].before(elem);
    row = row.nextElementSibling;
  }

  let coefficients = document.createElement('td');
  coefficients.style.background = '#9ca3af';

  coefficients.innerHTML = `<div class="flex"><div class="relative w-full"><input class="block w-full border disabled:cursor-not-allowed disabled:opacity-50 bg-gray-50 border-gray-300 text-gray-900 focus:border-cyan-500 focus:ring-cyan-500 dark:border-gray-600 dark:bg-gray-700 dark:text-white dark:placeholder-gray-400 dark:focus:border-cyan-500 dark:focus:ring-cyan-500 rounded-lg p-2.5 text-sm" type="text" name="coefficients" value="0"></div></div>`;
  document.querySelector('.anchor').before(coefficients);
}

export function removeColumn() {
  if (columnCount <= 2) {
    return;
  } else {
    --columnCount;
  }

  let table = document.querySelector('.inputTable').children[0];
  let vars = document.querySelector('.vars');

  table.children[0].children[1].colSpan = columnCount;
  vars.children[vars.children.length - 1].remove();

  let row = vars.nextElementSibling;
  for (let i = 0; i < rowCount; i++) {
    row.children[row.children.length - 2].remove();
    row = row.nextElementSibling;
  }

  document
    .querySelector('.coefficients')
    .children[
      document.querySelector('.coefficients').children.length - 2
    ].remove();
}
