/** @type {import('tailwindcss').Config} */
export default {
  content: [
    './index.html',
    './src/**/*.{js,jsx}',
    'node_modules/flowbite-react/**/*.{js,jsx,ts,tsx}',
  ],
  theme: {
    extend: {
      colors: {
        // green: '#1db954',
        'gray-100': '#b3b3b3',
        'gray-200': '#535353',
        'gray-300': '	#212121',
        dark: '#121212',
      },
    },
  },
  plugins: [require('flowbite/plugin')],
};
